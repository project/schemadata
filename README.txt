INTRODUCTION
------------

When we are working on local system then we can check database
easily. Some time its require to see the database frequently.
If we are developing application by multiple server then its very
difficult to login again and again and check weather the database entry is
done or not. This module provides you a page to check all the mysql tables
and its data.

Below links are given to display all the tables.

admin/structure/show-tables-names


INSTALLATION
------------
 
 * Install as usual, see http://drupal.org/node/895232 for further information.

CONFIGURATION
-------------

* The module has no menu or modifiable settings. There is no configuration. When
  enabled, the module will prevent the links from appearing. To get the links
  back, disable the module and clear caches.

* Configure user permissions in Administration » People » Permissions:

Author
------
Rajveer Gangwar
rajveer.gang@gmail.com